// MONGODB AGGREGATION

db.course_bookings.insertMany([
    {
        "courseId" : "C001", 
        "studentId": "S004", 
        "isCompleted": true
    },
    {
        "courseId" : "C002", 
        "studentId": "S001", 
        "isCompleted": false
    },
    {
        "courseId" : "C001", 
        "studentId": "S003", 
        "isCompleted": true
    },
    {
        "courseId" : "C003", 
        "studentId": "S002", 
        "isCompleted": false
    },
    {
        "courseId" : "C001", 
        "studentId": "S003", 
        "isCompleted": true
    },
    {
        "courseId" : "C004", 
        "studentId": "S004", 
        "isCompleted": false
    },
    {
        "courseId" : "C002", 
        "studentId": "S007", 
        "isCompleted": true
    },
    {
        "courseId" : "C003", 
        "studentId": "S005", 
        "isCompleted": false
    },
    {
        "courseId" : "C001", 
        "studentId": "S008", 
        "isCompleted": true
    },
    {
        "courseId" : "C004", 
        "studentId": "S0013", 
        "isCompleted": false
    }
]);


// Aggregation allows us to retrieve a group of data based on specific conditions. In this case, we are retrieving or grouping the data inside our course_bookings table and getting the totaL count of all the data inside of it
db.course_bookings.aggregate([
		{
			$group: {_id: null, count: {$sum:1} }
		}
	]);


// para macount kung ilan per courseId---------------------
db.course_bookings.aggregate([
		{
			$group: {_id: "$courseId", count: {$sum:1} }
		}
	]);



// ------------------------------------------------------------------

// $match - is a condition that has to be met in order for mongoDB to return data. In this case, we are trying to get all the fields where 'isCompleted' is equal to true

db.course_bookings.aggregate([
		{
			$match: {"isCompleted": true}
		},
		{
			group: {_id: "$courseId", total: {$sum:1} }
		}
	])

// (_id, total, count) example lang ng labeling. hindi siya yung mismong command



// -------------------------------------------------------------

// $project - either shows or doesnt show a field depending if you put 1 or 0 as its value. (parang inclussion and exclussion)

db.course_bookings.aggregate([
		{
			$match: {"isCompleted": true}
		},
		{
			$project: {"studentId": 1}   
		}
	])


// -------------------------------------------------------------

// $sort - sorts the return data in either ascending or descending order. (1 = ascending, -1 = descending)

db.course_bookings.aggregate([
		{
			$match: {"isCompleted": true}
		},
		{
			$sort: {"courseId": -1}
		}
	]);


// Mini-activity-------------------------------------------------

db.course_bookings.aggregate([
		{
			$match: {"isCompleted" : true},
		},
		{
			$sort: {"courseId": -1, "studentId": 1}
		}
	])


// -------------------------------------------------------------

db.orders.insertMany([
	{
		"customer_Id": "A123",
		"amount": 500,
		"status": "A"
	},
	{
		"customer_Id": "A123",
		"amount": 250,
		"status": "A"
	},
	{
		"customer_Id": "B212",
		"amount": 200,
		"status": "A"
	},
	{
		"customer_Id": "B212",
		"amount": 200,
		"status": "D"
	},
]);



// Operators are a way to do automatic calculations within our query.
/*
	1. $sum - gets the total of everything
	2. $max - gets the highest value out of everything else
	3. $min - gets the lowest value out of everything else
	4. $avg - gets the average of all the included fields
*/
db.orders.aggregate([
		{
			$match: {"status": "A"}
		},
		{
			$group: {_id: "$customer_Id", maxAmount: {$max: "$amount"}}
		}
	]);
